# Titre du projet

METEYER - Starcraft DM

### Prérequis

Lancer un composer require pour installer les modules suivants :
 -mikecao/flight
 -rmccue/requests
 -twig/twig
 -michelf/php-markdown

### Pour démarrer la page


1. Lancer Visual Studio Code puis mettre l'affichage du terminal
2. Installer les modules composer
3. Taper "index.php" pour vérifier que la page soit bien chargée
4. Taper "run.sh" afin de host la page sur l'adresse locale
5. Lancer un moteur de recherche puis taper l'adresse "127.0.0.1:**5001**". La page s'affichera

## Auteur

Théo Meteyer - https://gitlab.com/tmeteyer/meteyer-starcraft-dm

## Autre

Vu que c'est rare que quelqu'un ouvre un README, tu mérites donc de voir ce magnifique GIF : 
https://i.pinimg.com/originals/84/53/70/8453701c5c6c66fcf9b96043f73cf4d8.gif